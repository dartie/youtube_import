import os, sys, re, webbrowser

file = 'subscription_manager.xml'
url = 'https://www.youtube.com/channel/'
incognito = True
chrome_path = 'C:/Program Files (x86)/Google/Chrome/Application/chrome.exe %s --incognito'


with open(file, 'r') as read_channels:
    file_content = read_channels.read()

regex_channels = re.compile(r'channel_id=(.*?)"')

channel_list = regex_channels.findall(file_content)

for ychannel in channel_list:
    channel_url = url + ychannel
    if incognito:
        webbrowser.get(chrome_path).open_new(channel_url)
    else:
        webbrowser.open(channel_url)
